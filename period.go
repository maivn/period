package period

import (
	"fmt"
	"sort"
	"time"
)

type Periods []*Period

type Period struct {
	start time.Time
	end   time.Time
}

func NewPeriod(start, end time.Time) Period {
	p := Period{start: start, end: end}
	if start.Unix() > end.Unix() {
		p = Period{start: end, end: start}
	}
	return p
}

func (period *Period) Start() time.Time {
	return period.start
}

func (period *Period) End() time.Time {
	return period.end
}

type Keys []int64

func (keys Keys) Len() int           { return len(keys) }
func (keys Keys) Swap(i, j int)      { keys[i], keys[j] = keys[j], keys[i] }
func (keys Keys) Less(i, j int) bool { return keys[i] < keys[j] }

func (periods *Periods) prepare() {
	periods.sort()

	l := len(*periods)
	for i := 0; i < l-1; i++ {
		p := (*periods)[i].Union((*periods)[i+1])
		if len(p) == 1 {
			(*periods)[i+1] = (*periods)[i]
			*(*periods)[i] = *p[0]
		}
	}

	periods.unique()
	periods.sort()
}

func (periods *Periods) unique() {
	m := make(map[string]*Period)
	for _, p := range *periods {
		m[fmt.Sprintf("%d:%d", p.Start().Unix(), p.End().Unix())] = p
	}

	b := Periods{}
	for _, p := range m {
		b = append(b, p)
	}
	*periods = b
}

func (periods *Periods) sort() {
	var keys Keys
	m := make(map[int64]*Period)

	for _, p := range *periods {
		key := p.Start().Unix()
		m[key] = p
		keys = append(keys, key)
	}

	sort.Sort(keys)

	b := Periods{}
	for _, key := range keys {
		b = append(b, m[key])
	}

	*periods = b
}

func (period *Period) Union(b *Period) Periods {
	var periods Periods
	switch true {
	case period.Start().Unix() == period.End().Unix() && b.Start().Unix() == b.End().Unix():
	case period.Start().Unix() == period.End().Unix():
		periods = append(periods, b)
	case b.Start().Unix() == b.End().Unix():
		periods = append(periods, period)
	case period.Start().Unix() <= b.Start().Unix() && b.End().Unix() <= period.End().Unix():
		periods = append(periods, period)
	case b.Start().Unix() <= period.Start().Unix() && period.End().Unix() <= b.End().Unix():
		periods = append(periods, b)
	case b.End().Unix() < period.Start().Unix():
		periods = append(periods, b, period)
	case period.End().Unix() < b.Start().Unix():
		periods = append(periods, period, b)
	case b.End().Unix() == period.Start().Unix() || (b.Start().Unix() < period.Start().Unix() && period.Start().Unix() < b.End().Unix()):
		periods = append(periods, &Period{start: b.Start(), end: period.End()})
	case period.End().Unix() == b.Start().Unix() || (b.Start().Unix() < period.End().Unix() && period.End().Unix() < b.End().Unix()):
		periods = append(periods, &Period{start: period.Start(), end: b.End()})
	}
	return periods
}

func (period *Period) Intersection(b *Period) Periods {
	var periods Periods
	switch true {
	case b.End().Unix() <= period.Start().Unix() || period.End().Unix() <= b.Start().Unix():
	default:
		maxStart := period.Start()
		if maxStart.Unix() < b.Start().Unix() {
			maxStart = b.Start()
		}

		minEnd := period.End()
		if minEnd.Unix() > b.End().Unix() {
			minEnd = b.End()
		}

		periods = append(periods, &Period{start: maxStart, end: minEnd})
	}
	return periods
}

func (period *Period) Subtract(b *Period) Periods {
	var periods Periods
	switch true {
	case b.Start().Unix() <= period.Start().Unix() && period.End().Unix() <= b.Start().Unix():
	case b.End().Unix() <= period.Start().Unix() || period.End().Unix() <= b.Start().Unix():
		periods = append(periods, period)
	case period.Start().Unix() < b.Start().Unix() && b.End().Unix() < period.End().Unix():
		periods = append(periods, &Period{start: period.Start(), end: b.Start()}, &Period{start: b.End(), end: period.End()})
	case period.Start().Unix() < b.Start().Unix():
		periods = append(periods, &Period{start: period.Start(), end: b.Start()})
	case b.End().Unix() < period.End().Unix():
		periods = append(periods, &Period{start: b.End(), end: period.End()})
	}
	return periods
}

func (period *Period) Minutes() int64 {
	return (period.end.Unix() - period.start.Unix()) / 60
}

func (periods *Periods) Minutes() int64 {
	var m int64
	for _, period := range *periods {
		m = m + period.Minutes()
	}
	return m
}

func (period *Period) OffsetMinutes(minutes int64) {
	period.start.Add(time.Duration(minutes) * time.Minute)
	period.end.Add(time.Duration(minutes) * time.Minute)
}

func (periods *Periods) OffsetMinutes(minutes int64) {
	for i := range *periods {
		(*periods)[i].OffsetMinutes(minutes)
	}
}

func (periods *Periods) Union(b Periods) Periods {
	res := append(*periods, b...)

	res.prepare()
	return res
}

func (periods *Periods) Intersection(b Periods) Periods {
	var res Periods
	for _, ap := range *periods {
		for _, bp := range b {
			res = append(res, ap.Intersection(bp)...)
		}
	}

	res.prepare()
	return res
}

func (periods *Periods) Subtract(b Periods) Periods {
	res := *periods

	for _, bp := range b {
		var tmp Periods
		for _, ap := range res {
			tmp = append(tmp, ap.Subtract(bp)...)
		}
		res = tmp
	}

	res.prepare()
	return res
}
